(
    dev-libs/tevent[~scm]
    media-video/rtmpdump[~scm]
    net-analyzer/fail2ban[~scm]
    net-im/bitlbee[~scm]
    net-im/bitlbee-steam[~scm]
    net-irc/weechat[~scm]
    net-irc/znc[~scm]
    net-libs/jreen[~scm]
    net-libs/libndp[~scm]
    net-libs/miniupnpc[~scm]
    net-misc/connman[~scm]
    net-misc/mosh[~scm]
    net-p2p/transmission[~scm]
    net-proxy/torsocks[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

net-fs/cifs-utils[<=5.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Apr 2012 ]
    token = security
    description = [ CVE-2012-1586 ]
]]

net-misc/tor[<0.2.2.39] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 20 Sep 2012 ]
    token = security
    description = [ Several security problems ]
]]

web-apps/cgit[<0.9.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 30 May 2013 ]
    token = security
    description = [ CVE-2013-2117 ]
]]

net-libs/libssh[<0.9.6] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 26 Aug 2020 ]
    token = security
    description = [ CVE-2021-3634 ]
]]

www-servers/nginx[<1.20.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 May 2021 ]
    token = security
    description = [ CVE-2021-23017 ]
]]

www-servers/nginx[>=1.23.0] [[
    author = [ Lucas Roberto <sigmw@protonmail.com> ]
    date = [ 05 July 2022 ]
    token = testing
    description = [ Not stable yet ]
]]

net-libs/libmicrohttpd[<0.9.32] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Dec 2013 ]
    token = security
    description = [ CVE-2013-703{8,9} ]
]]

net-proxy/torsocks[<2] [[
    author = [ Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu> ]
    date = [ 19 Jan 2013 ]
    token = security
    description = [ See https://lists.torproject.org/pipermail/tor-dev/2013-June/004959.html ]
]]

net-remote/FreeRDP[<2.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Jul 2020 ]
    token = security
    description = [ CVE-2020-15103 ]
]]

app-crypt/krb5[<1.19.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Mar 2022 ]
    token = security
    description = [ CVE-2021-37750 ]
]]

net-proxy/squid[<5.6] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2022 ]
    token = security
    description = [ CVE-2021-46784 ]
]]

net-misc/openvpn[<2.4.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jun 2017 ]
    token = security
    description = [ CVE-2017-7508, CVE-2017-7520 CVE-2017-7521, CVE-2017-7522 ]
]]

net-analyzer/tcpdump[<4.99.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Jan 2021 ]
    token = security
    description = [ CVE-2020-8037 ]
]]

net-analyzer/wireshark[<3.6.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Feb 2022 ]
    token = security
    description = [ wnpa-sec-2022-{1,2,3,4,5} ]
]]

net/net-snmp[<5.7.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Jul 2015 ]
    token = security
    description = [ CVE-2014-3565 ]
]]

web-apps/cgit[<0.12] [[
    author = [ Kylie McClain <somasis@exherbo.org> ]
    date = [ 14 Jan 2015 ]
    token = security
    description = [ CVE-2016-1899, CVE-2016-1900, CVE-2016-1901 ]
]]

net-misc/socat[<1.7.3.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 01 Feb 2015 ]
    token = security
    description = [ http://www.dest-unreach.org/socat/contrib/socat-secadv7.html
                    http://www.dest-unreach.org/socat/contrib/socat-secadv8.html ]
]]

(
    dev-db/mariadb[<10.3.31]
    dev-db/mariadb[>=10.4&<10.4.21]
    dev-db/mariadb[>=10.5&<10.5.12]
    dev-db/mariadb[>=10.6&<10.6.4]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 12 Aug 2021 ]
    *token = security
    *description = [ CVE-2021-2372, CVE-2021-2389 ]
]]

net-fs/samba[<4.16.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Jul 2022 ]
    token = security
    description = [ CVE-2022-{2031,32742,32744,32745,32746} ]
]]

net-mail/dovecot[<2.3.19.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Jul 2022 ]
    token = security
    description = [ CVE-2022-30550 ]
]]

www-servers/apache[<2.4.54] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 June 2022 ]
    token = security
    description = [ CVE-2022-{26377,28614,28615,29404,30522,30556,31813} ]
]]

net-irc/weechat[<1.9.1] [[
    author = [ Johannes Nixdorf <mixi@exherbo.org> ]
    date = [ 30 Sep 2017 ]
    token = security
    description = [ CVE-2017-14727 ]
]]

media-video/rtmpdump[<2.4_p20151223] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2015-827{0,1,2} ]
]]

net-mail/tnef[<1.4.18] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Oct 2020 ]
    token = security
    description = [ CVE-2019-18849 ]
]]

net-wireless/hostapd[<2.9] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 15 Oct 2020 ]
    token = security
    description = [ CVE-2019-949{4,5,6,7,8}, CVE-2019-16275, CVE-2020-12695 ]
]]

net-remote/teamviewer[<15.21.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 26 Aug 2021 ]
    token = security
    description = [ CVE-2021-34858, CVE-2021-34859 ]
]]

sys-auth/sssd[<2.6.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 28 Oct 2021 ]
    token = security
    description = [ CVE-2021-3621 ]
]]

net/mosquitto[<2.0.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 16 Sep 2021 ]
    token = security
    description = [ CVE-2021-34434 ]
]]

net-libs/nghttp2[<1.41.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2020 ]
    token = security
    description = [ CVE-2020-11080 ]
]]

net-irc/znc[<1.8.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 08 Jun 2020 ]
    token = security
    description = [ CVE-2020-13775 ]
]]

net-p2p/transmission[<2.94] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Jul 2018 ]
    token = security
    description = [ CVE-2018-5702 ]
]]

net-analyzer/ettercap[<0.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Jan 2019 ]
    token = security
    description = [ CVE-2014-{6395,6396,9376,9377,9378,9379,9380,9381}, CVE-2017-6430 ]
]]

net/synapse[<1.61.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 05 Jul 2022 ]
    token = security
    description = [ CVE-2022-31052 ]
]]

net-libs/zeromq[<4.3.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 24 Sep 2020 ]
    token = security
    description = [ CVE-2020-15166 ]
]]

net/gitea[<1.16.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 04 Apr 2022 ]
    token = security
    description = [ CVE-2022-1058 ]
]]

net/gogs[<0.11.91] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 18 Nov 2019 ]
    token = security
    description = [ CVE-2019-14544 ]
]]

www-servers/lighttpd[<1.4.64] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Jan 2022 ]
    token = security
    description = [ CVE-2022-22707 ]
]]

sys-cluster/ceph[<14.2.20] [[
    author = [ Arnaud Lefebvre <a.lefebvre@outlook.fr> ]
    date = [ 15 May 2021 ]
    token = security
    description = [ CVE-2021-3509, CVE-2021-3524, CVE-2021-3531 ]
]]

dev-db/redis[<7.0.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2022 ]
    token = security
    description = [ CVE-2022-31144 ]
]]

www-servers/tomcat-bin[<8.5.75] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Feb 2022 ]
    token = security
    description = [ CVE-2022-23181 ]
]]

net/coturn[<4.5.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Jan 2021 ]
    token = security
    description = [ CVE-2020-26262 ]
]]

web-apps/mailman[<2.1.35] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 20 Oct 2021 ]
    token = security
    description = [ CVE-2021-42096, CVE-2021-42097 ]
]]

net/solr[<8.8.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 14 Jun 2021 ]
    token = security
    description = [ CVE-2021-27905, CVE-2021-29262, CVE-2021-29943 ]
]]

net-libs/libmaxminddb[<1.4.3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Nov 2020 ]
    token = security
    description = [ CVE-2020-28241 ]
]]

net-mail/dovecot-pigeonhole[<0.5.15] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 21 Jun 2021 ]
    token = security
    description = [ CVE-2020-28200 ]
]]

web-apps/mailman[>=3] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 22 Oct 2021 ]
    token = broken
    description = [ Needs testing, also misses postorious and hyperkitty for
                    web interface and archives ]
]]

web-apps/grafana[<9.0.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jul 2022 ]
    token = security
    description = [ CVE-2022-31097, CVE-2022-31107 ]
]]

web-apps/opensearch[<1.2.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 19 Jan 2022 ]
    token = security
    description = [ CVE-2020-15522, CVE-2021-{41269,43797,44832} ]
]]

net/openhab[<3.2.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Dec 2021 ]
    token = security
    description = [ CVE-2021-45105 ]
]]

web-apps/opensearch-logstash[<7.16.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 17 Feb 2022 ]
    token = security
    description = [ CVE-2021-44832 ]
]]

net/solr[<8.11.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 21 Dec 2021 ]
    token = security
    description = [ CVE-2021-44228 ]
]]

net/keycloak[<16.1.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 27 Dec 2021 ]
    token = security
    description = [ CVE-2021-4133 ]
]]

dev-libs/hiredis[<1.0.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2022 ]
    token = security
    description = [ CVE-2021-32765 ]
]]

web-apps/opensearch-dashboards[<2.1.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 11 Jul 2022 ]
    token = security
    description = [ CVE-2022-25851, CVE-2022-33987 ]
]]

(
    dev-scm/libgit2:1.3[<1.3.2]
    dev-scm/libgit2:1.4[<1.4.4]
) [[
    *author = [ Timo Gurr <tgurr@exherbo.org> ]
    *date = [ 13 Jun 2022 ]
    *token = security
    *description = [ CVE 2022-29187 ]
]]
