# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=greenbone tag=v${PV} ] \
    cmake

SUMMARY="OpenVAS scanner"
HOMEPAGE+=" https://www.openvas.org"

LICENCES="GPL-2 public-domain"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    snmp
"

DEPENDENCIES="
    build:
        sys-devel/bison
        virtual/pkg-config
    build+run:
        group/gvm
        user/gvm
        app-crypt/gpgme[>=1.1.2]
        core/json-glib[>=1.4.4]
        dev-libs/glib:2[>=2.42]
        dev-libs/gnutls[>=3.6.4]
        dev-libs/libbsd
        dev-libs/libgcrypt[>=1.6]
        dev-libs/libgpg-error
        dev-libs/libksba[>=1.0.7]
        dev-libs/libpcap
        net-libs/libssh[>=0.6.0]
        net-analyzer/gvm-libs[>=22.4]
        snmp? ( net/net-snmp )
    run:
        app-admin/sudo [[ note = [ Required to run Network Vulnerability Tests (NVTs) ] ]]
        net-misc/rsync [[ note = [ Required to update the Network Vulnerability Tests (NVTs) feed ] ]]
        net-scanner/nmap [[ note = [ The predefined scan configurations needs nmap as a port scanner ] ]]
    run+test:
        dev-db/redis[>=5.0.3]
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCLANG_FORMAT:BOOL=FALSE
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DDATADIR:PATH=/usr/share
    -DDOXYGEN_EXECUTABLE:BOOL=FALSE
    -DENABLE_COVERAGE:BOOL=FALSE
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DOPENVAS_RUN_DIR:PATH=/run/ospd
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'snmp SNMP'
)

src_install() {
    cmake_src_install

    insinto /etc/openvas
    doins "${FILES}"/openvas.conf

    keepdir /var/log/gvm
    edo chown gvm:gvm "${IMAGE}"/var/log/gvm

    # greenbone-nvt-sync
    keepdir /var/lib/openvas/plugins
    edo chown -R gvm:gvm "${IMAGE}"/var/lib/openvas
    keepdir /var/lib/notus
    edo chown -R gvm:gvm "${IMAGE}"/var/lib/notus

    # remove empty directories
    edo rmdir "${IMAGE}"/etc/openvas/gnupg
    edo rmdir "${IMAGE}"/var/lib/openvas/gnupg
    edo rmdir "${IMAGE}"/usr/share/openvas
}

