# Copyright 2009 Anders Ossowicki <arkanoid@exherbo.org>
# Copyright 2012 Luis Aranguren <mercurytoxic@luisaranguren.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A tool for network monitoring and data acquisition"
DESCRIPTION="
tcpdump is a command-line tool for capturing network packets on one or more interfaces. tcpdump
uses libpcap and has a wide range of options for displaying and filtering captured data. It can
also save captured packages to a file for later inspection either with tcpdump itself, or another
application.
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/release/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${HOMEPAGE}/tcpdump-changes.txt"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/#documentation"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    smb [[ description = [ Enable possibly-buggy SMB (samba fileshare) printer ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        user/tcpdump
        group/tcpdump
        dev-libs/libpcap[>=1.5.3]
        sys-libs/libcap-ng
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    suggestion:
        net-libs/libsmi [[ description = [ Provides access to SMI (Structure of Management Information) for SNMP ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-local-libpcap
    --with-cap-ng
    --with-crypto
    --with-user=tcpdump
    --without-sandbox-capsicum
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( smb )

