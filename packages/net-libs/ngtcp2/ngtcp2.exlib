# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.xz ] cmake

export_exlib_phases src_install

SUMMARY="Implementation of the QUIC protocol which is now being discussed in the IETF QUICWG"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    debug
    examples [[
        requires = [ providers: gnutls ]
    ]]
    ( providers: gnutls )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        examples? (
            dev-libs/libev[>=4.11]
            net-libs/nghttp3
        )
        providers:gnutls? ( dev-libs/gnutls[>=3.7.2] )
    test:
        dev-util/cunit[>=2.1]
"
# OpenSSL needs to be patched, cf.
# https://github.com/tatsuhiro-t/openssl/tree/openssl-quic-draft-23
#    build+run:
#        dev-libs/openssl[>=1.1.1]

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${PNVR}
    -DENABLE_BORINGSSL:BOOL=FALSE
    -DENABLE_OPENSSL:BOOL=FALSE
    -DENABLE_SHARED_LIB:BOOL=TRUE
    -DENABLE_STATIC_LIB:BOOL=FALSE
    -DENABLE_ASAN:BOOL=FALSE
    -DENABLE_WERROR:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    DEBUG
    'providers:gnutls GNUTLS'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'examples Libev'
    'examples Libnghttp3'
)

CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DCMAKE_DISABLE_FIND_PACKAGE_CUnit:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_CUnit:BOOL=TRUE'
)

ngtcp2_src_install() {
    cmake_src_install

    if option examples ; then
        dobin examples/gtls{client,server}
    fi
}

