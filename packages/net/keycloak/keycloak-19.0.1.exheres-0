# Copyright 2020-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=${PV} suffix=tar.gz ] \
    systemd-service

SUMMARY="Open Source Identity and Access Management"
DESCRIPTION="
Keycloak is an open source Identity and Access Management solution aimed at modern applications and
services. It makes it easy to secure applications and services with little to no code.
"
HOMEPAGE="https://www.${PN}.org"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/docs/latest/release_notes/index.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
    run:
        virtual/jre:*[>=1.8]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-17.0.0-defaults.patch
)

pkg_setup() {
    exdirectory --allow /opt
}

src_test() {
    :
}

src_install() {
    insinto /opt/${PN}
    doins -r *

    edo rm "${IMAGE}"/opt/keycloak/bin/*.bat

    dodir /usr/$(exhost --target)/bin
    dosym /opt/keycloak/bin/kcadm.sh /usr/$(exhost --target)/bin/kcadm
    dosym /opt/keycloak/bin/kcreg.sh /usr/$(exhost --target)/bin/kcreg

    edo chmod 0755 "${IMAGE}"/opt/${PN}/bin/*.sh

    local keepdirs=(
        /opt/keycloak/conf
        /opt/keycloak/data
    )
    for d in ${keepdirs[@]}; do
        keepdir $d
        edo chown keycloak:keycloak "${IMAGE}"/${d}
    done

    edo chown keycloak:keycloak "${IMAGE}"/opt/keycloak/conf/{cache-ispn.xml,keycloak.conf}
    edo chown -R keycloak:keycloak "${IMAGE}"/opt/keycloak/lib/quarkus

    install_systemd_files

    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
Z /opt/keycloak/conf - keycloak keycloak -
z /opt/keycloak/data - keycloak keycloak -
Z /opt/keycloak/lib/quarkus - keycloak keycloak -
EOF

    hereenvd 99keycloak <<EOF
CONFIG_PROTECT="/opt/keycloak/conf"
EOF
}

