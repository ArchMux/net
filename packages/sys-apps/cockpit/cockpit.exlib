# Copyright 2014-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=cockpit-project release=${PV} suffix=tar.xz ] \
    pam \
    python [ blacklist=none multibuild=false has_bin=true ] \
    systemd-service

export_exlib_phases src_install

SUMMARY="A sysadmin login session in a web browser"
DESCRIPTION="
Cockpit is an interactive server admin interface. It is easy to use and very light weight. Cockpit
interacts directly with the operating system from a real Linux session in a browser.

Cockpit makes Linux discoverable, allowing sysadmins to easily perform tasks such as starting
containers, storage administration, network configuration, inspecting logs and so on.

Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be
stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the
Cockpit journal interface.

On the Cockpit dashboard, you can easily add other machines with Cockpit installed that are
accessible via SSH.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    polkit
    (
        cockpit_components:
            client         [[ description = [ Cockpit GTK graphical user interface ] ]]
            networkmanager [[ description = [ Component for network management using NetworkManager ] ]]
            pcp            [[ description = [ Cockpit support for reading PCP metrics and loading PCP archives ] ]]
            storage        [[ description = [ Component for storage management supported by udisks:2 ] ]]
    )

    ( libc: musl )
"

# test-webservice fails when cockpit is already installed
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
        doc? (
            app-text/docbook-xsl-stylesheets
            app-text/xmlto
        )
    build+run:
        user/cockpit-ws
        user/cockpit-wsinstance
        group/cockpit-ws
        group/cockpit-wsinstance
        app-crypt/krb5[>=1.11]
        core/json-glib[>=1.4]
        dev-libs/glib:2[>=2.56]
        dev-libs/gnutls[>=3.6.0]
        gnome-desktop/libgudev[>=164]
        net-libs/libssh[>=0.8.5]
        sys-apps/systemd[>=235] [[ note = [ for logind ] ]]
        sys-libs/pam
        !libc:musl? ( dev-libs/libxcrypt:= )
        cockpit_components:pcp? ( monitor/pcp )
        polkit? ( sys-auth/polkit:1[>=0.105] )
    run:
        dev-libs/glib-networking[gnutls(+)][ssl(+)]
        cockpit_components:client? (
            gnome-desktop/gobject-introspection:1
            net-libs/webkit:4.0[gobject-introspection]
            x11-libs/gtk+:3[>=3.0][gobject-introspection]
        )
        cockpit_components:networkmanager? ( net-apps/NetworkManager[>=0.9.7.0] )
        cockpit_components:storage? (
            dev-python/dbus-python[python_abis:*(-)?]
            sys-apps/udisks:2[>=2.6.4]
        )
    recommendation:
        app-crypt/sscg[>=2.3.0] [[
            description = [ Use SSCG for generating self-signed certificates ]
        ]]
    suggestion:
        dev-libs/libpwquality [[
            description = [ Adds support to display the password strength when creating users ]
        ]]
        cockpit_components:storage? ( net-fs/nfs-utils ) [[
            description = [ Adds support to manage NFS mounts ]
        ]]
        sys-auth/sssd [[
            description = [ Adds support for client certificate/smart card authentication ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-ssh
    --disable-selinux-policy
    --with-admin-group=wheel
    --with-cockpit-user=cockpit-ws
    --with-cockpit-ws-instance-user=cockpit-wsinstance
    --with-pamdir=$(getpam_mod_dir)
    --with-systemdunitdir=${SYSTEMDSYSTEMUNITDIR}
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    doc
    "cockpit_components:client cockpit-client"
    "cockpit_components:pcp pcp"
    polkit
)

cockpit_src_install() {
    default

    # certificates
    keepdir /etc/cockpit/ws-certs.d

    # machines
    keepdir /etc/cockpit/machines.d

    # configuration
    keepdir /var/lib/cockpit

    # pam configuration
    pamd_mimic_system cockpit auth auth account session

    # remove Fedora firewalld stuff
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/firewalld

    # remove branding for other distributions
    edo rm -rf "${IMAGE}"/usr/share/cockpit/branding/{arch,centos,debian,fedora,kubernetes,registry,rhel,ubuntu}

    # remove debug data for stuff in /usr/share
    # https://github.com/cockpit-project/cockpit/issues/3288
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/src

    # cockpit packages
    # remove Applications component (uses PackageKit and AppStream)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/apps
    # remove Kernel Dump component (while we have kexec-tools it requires a running kdump.service, see Fedora sources)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/kdump
    # remove PackageKit component (PackageKit lacks a Paludis backend)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/packagekit
    # remove Playground component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/playground
    # remove SELinux component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/selinux
    # remove Diagnostic reports component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/sosreport
    # remove Tuned component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/tuned

    option cockpit_components:client || edo rm -rf "${IMAGE}"/usr/share/cockpit/client
    option cockpit_components:networkmanager || edo rm -rf "${IMAGE}"/usr/share/cockpit/networkmanager
    option cockpit_components:pcp || edo rm -rf "${IMAGE}"/usr/share/cockpit/pcp
    option cockpit_components:storage || edo rm -rf "${IMAGE}"/usr/share/cockpit/storaged
}

