# Copyright 2015-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="A wrapper for DNS name resolving or DNS faking"
DESCRIPTION="
* Redirects name queries to the nameservers specified in your resolv.conf.
* Can fake DNS queries using a simple formatted DNS hosts file.

resolv_wrapper makes it possible on most UNIX platforms to contact your own DNS implmentation in
your test environment. It requires socket_wrapper to be able to contact the server. Alternatively,
the wrapper is able to fake DNS queries and return valid responses to your application.
"
HOMEPAGE="https://cwrap.org/${PN}.html"
DOWNLOADS="mirror://samba/../cwrap/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# leaves hanging dns_srv processes, last checked: 1.1.7
RESTRICT="test"

DEPENDENCIES="
    test:
        dev-util/cmocka[>=1.1.0]
"

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DUNIT_TESTING:BOOL=TRUE -DUNIT_TESTING:BOOL=FALSE'
)

src_test() {
    esandbox allow_net "unix:/tmp/test_resolv_wrapper_*/*"

    default

    esandbox disallow_net "unix:/tmp/test_resolv_wrapper_*/*"
}

