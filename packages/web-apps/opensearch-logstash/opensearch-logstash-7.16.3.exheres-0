# Copyright 2021-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Server-side data processing pipeline"
HOMEPAGE="https://opensearch.org/"
DOWNLOADS="https://artifacts.opensearch.org/logstash/logstash-oss-with-opensearch-output-plugin-${PV}-linux-x64.tar.gz"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip"

DEPENDENCIES="
    build+run:
        group/opensearch-logstash
        user/opensearch-logstash
"

WORK=${WORKBASE}/${PNV/opensearch-/}

src_test() {
    :
}

src_install() {
    insinto /etc/opensearch-logstash
    doins -r config/*

    insinto /usr/share/opensearch-logstash
    doins -r {bin,jdk,lib,logstash-core,logstash-core-plugin-api,modules,tools,vendor}
    doins {Gemfile,Gemfile.lock}

    keepdir /var/{lib,log}/opensearch-logstash
    # required to prevent startup error
    keepdir /usr/share/opensearch-logstash/data

    edo chmod 0755 "${IMAGE}"/usr/share/opensearch-logstash/{jdk/,}bin/*
    edo chmod 0755 "${IMAGE}"/usr/share/opensearch-logstash/jdk/lib/{jexec,jspawnhelper}
    edo chown -R opensearch-logstash:opensearch-logstash "${IMAGE}"/etc/opensearch-logstash
    edo chown opensearch-logstash:opensearch-logstash "${IMAGE}"/var/{lib,log}/opensearch-logstash

    install_systemd_files

    # logstash.yml
    edo sed \
        -e 's@# path.data:@path.data: /var/lib/opensearch-logstash/data@g' \
        -e 's@# path.logs:@path.logs: /var/log/opensearch-logstash@g' \
        -i "${IMAGE}"/etc/opensearch-logstash/logstash.yml
}

