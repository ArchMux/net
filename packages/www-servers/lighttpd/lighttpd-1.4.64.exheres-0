# Copyright 2008 Richard Brown
# Distributed under the terms of the GNU General Public License v2

require systemd-service flag-o-matic \
    lua [ whitelist='5.1 5.2 5.3 5.4' multibuild=false with_opt=true ] \
    autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Lightweight, fast, secure, compliant, flexible,  web-server"
DESCRIPTION="
A secure, fast, compliant and very flexible web-server which has been optimized for
high-performance environments. It has a very low memory footprint compared to other webservers and
takes care of cpu-load. Its advanced feature-set (FastCGI, CGI, Auth, Output-Compression,
URL-Rewriting and many more) make lighttpd the perfect webserver-software for every server that is
suffering load problems.
"
HOMEPAGE="https://${PN}.net"
DOWNLOADS="https://download.${PN}.net/${PN}/releases-$(ever range 1-2).x/${PNV}.tar.xz"

UPSTREAM_CHANGELOG="https://redmine.${PN}.net/repositories/entry/${PN}/tags/${PNV}/NEWS"
UPSTREAM_DOCUMENTATION="https://redmine.${PN}.net/projects/${PN}/wiki"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    fam
    libunwind [[ description = [ Include libunwind support for backtraces on assert failures ] ]]
    lua
    pcre
    ssl
    webdav    [[ description = [ Enables webdev properties ] ]]
    xattr     [[ description = [ Enable extended attributes support ] ]]

    ssl? ( ( providers: libressl openssl ) [[ number-selected = exactly-one ]] )
    ( libc: musl )
"
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        user/${PN}
        group/${PN}
        app-arch/bzip2
        dev-libs/xxHash
        sys-libs/pam
        sys-libs/zlib
        debug? ( dev-util/valgrind )
        fam? ( app-admin/gamin )
        !libc:musl? ( dev-libs/libxcrypt:= )
        libunwind? ( dev-libs/libunwind )
        pcre? ( dev-libs/pcre2 )
        ssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl:= )
        )
        webdav? (
            dev-db/sqlite:3
            dev-libs/libxml2:2.0
            sys-fs/e2fsprogs
        )
        xattr? ( sys-apps/attr )
"

#FIXME missing packages
#FIXME test would like fcgi

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --libdir=/usr/$(exhost --target)/lib/${PN}
    --enable-ipv6
    --with-bzip2
    --with-libev
    --with-pam
    --with-xxhash
    --with-zlib
    --without-brotli
    --without-dbi
    --without-gnutls
    --without-krb5
    --without-ldap
    --without-libev
    --without-maxminddb
    --without-mbedtls
    --without-mysql
    --without-nettle
    --without-nss
    --without-pcre
    --without-pgsql
    --without-sasl
    --without-wolfssl
    --without-zstd
)

DEFAULT_SRC_CONFIGURE_OPTIONS=(
    "pcre PCRE2CONFIG=/usr/$(exhost --target)/bin/pcre2-config"
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "debug valgrind"
    fam
    libunwind
    lua
    "pcre pcre2"
    "ssl openssl"
    "webdav webdav-locks"
    "webdav webdav-props"
    "xattr attr"
)

pkg_setup() {
    exdirectory --allow /srv
}

src_prepare() {
    edo sed \
        -e '/^#server.pid-file[ \t]*=/s:^#::' \
        -e '/^#server.username[ \t]*=/{s:^#::;s:\"wwwrun\":\"lighttpd\":;}' \
        -e '/^#server.groupname[ \t]*=/{s:^#::;s:\"wwwrun\":\"lighttpd\":;}' \
        -i doc/config/lighttpd.conf

    # respect selected LUA_ABI
    edo sed \
        -e "s:\$luaname:lua-${LUA_ABIS}:g" \
        -i configure.ac

    autotools_src_prepare
}

src_configure() {
    CC_FOR_BUILD="$(exhost --build)-cc" \
    CFLAGS_FOR_BUILD="$(print-build-flags CFLAGS)" \
    CPPFLAGS_FOR_BUILD="$(print-build-flags CPPFLAGS)" \
    LDFLAGS_FOR_BUILD="$(print-build-flags LDFLAGS)" \
        default
}

src_install() {
    default

    insinto /etc/lighttpd
    doins doc/config/*.conf

    insinto /etc/lighttpd/conf.d
    doins doc/config/conf.d/*.conf

    dodoc doc/config/vhosts.d/vhosts.template

    keepdir /var/log/lighttpd
    keepdir /srv/www/htdocs

    edo chown lighttpd:lighttpd "${IMAGE}"/var/log/lighttpd
    edo chmod 0750 "${IMAGE}"/var/log/lighttpd

    insinto "${SYSTEMDSYSTEMUNITDIR}"
    doins doc/systemd/lighttpd.service
}

